package com.chef;

import java.util.Arrays;

public class Sorter {
    private final String[] args;

    public Sorter(String[] args) {
        if (args.length == 0) {
            throw new IllegalArgumentException("There's no values, please input some.");
        } else if (args.length > 10) {
            throw new IllegalArgumentException("This sorter takes up to ten values!");
        }

        this.args = args;
    }

    public Integer[] sort() {
        Integer[] numbers = new Integer[this.args.length];

        try {
            if (this.args.length == 1)
                return new Integer[]{Integer.parseInt(this.args[0])};

            for (int i = 0; i < this.args.length; i++) {
                numbers[i] = Integer.parseInt(args[i]);
            }
        } catch (NumberFormatException e) {
            System.out.println("Possibly not a number: " + e.getMessage());
        }

        Arrays.sort(numbers);
        return numbers;
    }
}
