package com.chef;

import java.util.Arrays;

/**
 * Small Java application that takes up to ten command-line arguments as integer values,
 * sorts them in the ascending order, and then prints them into standard output.
 */
public class App 
{
    /**
     * Runs the main program, this creates a Sorter that sorts numbers
     * @param args List of numbers to sort, up to 10 numbers are accepted,
     *             if input can't be converted into {@link java.lang.Integer},
     *             it'll throw an {@link java.lang.IllegalArgumentException}
     */
    public static void main( String[] args )
    {
        Sorter sorter = new Sorter(args);
        System.out.println(Arrays.toString(sorter.sort()));
    }
}
