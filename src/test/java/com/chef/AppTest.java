package com.chef;

import org.junit.Test;

import java.util.Random;

/**
 * Unit test for simple App.
 */
public class AppTest {
    Random random = new Random();

    /**
     * Test when method runs with no args
     */
    @Test (expected = IllegalArgumentException.class)
    public void testNoArgs() {
        String[] noArgs = new String[0];
        App.main(noArgs);
    }

    /**
     * Test for one argument case
     */
    @Test
    public void testOneArg() {
        String[] arguments = new String[]{String.valueOf(random.nextInt())};
        App.main(arguments);
    }

    /**
     * Test for ten arguments case
     */
    @Test
    public void testTenArgs() {
        String[] arguments = new String[10];
        for (int i = 0; i < 10; i++) {
            arguments[i] = String.valueOf(random.nextInt());
        }

        App.main(arguments);
    }

    /**
     * Test for more than 10 arguments case
     */
    @Test (expected = IllegalArgumentException.class)
    public void testMoreThanTenArgs() {
        int size = random.nextInt(100) + 10;
        String[] arguments = new String[size];

        for (int i = 0; i < size; i++) {
            arguments[i] = String.valueOf(random.nextInt());
        }

        App.main(arguments);
    }
}
